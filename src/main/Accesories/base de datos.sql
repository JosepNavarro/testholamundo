CREATE DATABASE  IF NOT EXISTS chispas /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE chispas;

CREATE TABLE `gift_certificate` (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)NOT NULL,
    description TEXT NOT NULL,
    price DECIMAL(10, 2),
    duration INT(6)NOT NULL,
    create_date DATE,
    last_update_date DATE
);

CREATE TABLE tag (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE giftCertificate_Tag (
    gift_certificate_id INT,
    tag_id INT,
    PRIMARY KEY (gift_certificate_id, tag_id),
    FOREIGN KEY (gift_certificate_id) REFERENCES gift_certificate(id),
    FOREIGN KEY (tag_id) REFERENCES tag(id)
);

INSERT INTO `gift_certificate` (name, description, price, duration, create_date, last_update_date)
VALUES 
('Certificado 1 Perico', 'Perico es un buen programador', 100.00, 30, '2024-01-01', '2024-01-01'),
('Certificado 2 Raul', 'Raul es un gran padre', 200.00, 60, '2024-02-01', '2024-02-01'),
('Certificado 3 Clodomiro', 'Clodomiro es el mejor ayudante', 300.00, 90, '2024-03-01', '2024-03-01'),
('Certificado 4 Maria', 'Maria hace el mejor cafe', 400.00, 120, '2024-04-01', '2024-04-01'),
('Certificado 5 Marta', 'Marta sabe de ecuaciones', 500.00, 150, '2024-05-01', '2024-05-01');

INSERT INTO `tag` (name)
VALUES 
('Progracion'),
('Familia'),
('Ferreteria'),
('Cocina'),
('Matematicas');

-- Aquí necesitarás actualizar los valores de gift_certificate_id y tag_id para que coincidan con los ID generados automáticamente
INSERT INTO `giftCertificate_Tag` (gift_certificate_id, tag_id)
VALUES 
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 4),
(3, 1),
(3, 2),
(3, 3),
(4, 2),
(4, 4),
(4, 5),
(5, 1),
(5, 2),
(5, 3),
(5, 5);