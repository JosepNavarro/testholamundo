package com.epam.esm.dao;

import com.epam.esm.model.GiftCertificate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Clase GiftCertificateDao para interactuar con la base de datos y realizar operaciones CRUD en la tabla 'gift_certificate'.
 */
@Component
public class GiftCertificateDao {
    private JdbcTemplate jdbcTemplate;

    /**
     * Constructor de GiftCertificateDao.
     *
     * @param jdbcTemplate El objeto JdbcTemplate para interactuar con la base de datos.
     */
    public GiftCertificateDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Busca un GiftCertificate por su ID.
     *
     * @param id El ID del GiftCertificate a buscar.
     * @return Un Optional que puede contener el GiftCertificate si se encuentra.
     */
    public Optional<GiftCertificate> findById(int id) {
        String sql = "SELECT * FROM gift_certificate WHERE id = ?";
        List<GiftCertificate> certificates = jdbcTemplate.query(sql,
                ps -> ps.setInt(1, id),
                (rs, rowNum) -> GiftCertificate.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .description(rs.getString("description"))
                        .duration(rs.getInt("duration"))
                        .createDate(rs.getDate("create_date"))
                        .lastUpdateDate(rs.getDate("last_update_date"))
                        .build());

        return certificates.stream().findFirst();
    }

    /**
     * Inserta un nuevo GiftCertificate en la base de datos.
     *
     * @param giftCertificate El GiftCertificate a insertar.
     * @return El número de filas afectadas.
     */
    public int insert(GiftCertificate giftCertificate) {
        if(giftCertificate.getName().length() > 255 )  {
            throw new IllegalArgumentException("No puede tener mas de 255 caracteres");
        }
        String sql = "INSERT INTO gift_certificate (name, description, duration, create_date, last_update_date) VALUES (?, ?, ?, ?, ?)";
        return jdbcTemplate.update(
                sql,
                giftCertificate.getName(),
                giftCertificate.getDescription(),
                giftCertificate.getDuration(),
                giftCertificate.getCreateDate(),
                giftCertificate.getLastUpdateDate());
    }


    /**
     * Actualiza un GiftCertificate existente en la base de datos.
     *
     * @param giftCertificate El GiftCertificate con los datos actualizados.
     * @return El número de filas afectadas.
     */

    public int update(GiftCertificate giftCertificate) {
        if(giftCertificate.getName().length() > 255 )  {
            throw new IllegalArgumentException("No puede tener mas de 255 caracteres");
        }
        String sql = "UPDATE gift_certificate SET name = ?, description = ?, duration = ?, create_date = ?, last_update_date = ? WHERE id = ?";
        return jdbcTemplate.update(
                sql,
                giftCertificate.getName(),
                giftCertificate.getDescription(),
                giftCertificate.getDuration(),
                giftCertificate.getCreateDate(),
                giftCertificate.getLastUpdateDate(),
                giftCertificate.getId());
    }

    /**
     * Elimina un GiftCertificate de la base de datos por su ID.
     *
     * @param id El ID del GiftCertificate a eliminar.
     * @return El número de filas afectadas.
     */

    public int deleteById(int id) {
        String sql = "DELETE FROM gift_certificate WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
    /**
     * Devuelve una lista de todos los GiftCertificates.
     *
     * @return Una lista de GiftCertificates.
     */
    public List<GiftCertificate> findAllGiftCerticates() {
        String sql = "SELECT * FROM gift_certificate";
        List<GiftCertificate> certificates = jdbcTemplate.query(sql,
                (rs, rowNum) -> GiftCertificate.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .description(rs.getString("description"))
                        .duration(rs.getInt("duration"))
                        .createDate(rs.getDate("create_date"))
                        .lastUpdateDate(rs.getDate("last_update_date"))
                        .build());

        return certificates;
    }
}
