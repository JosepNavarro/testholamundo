package com.epam.esm.dao;

import com.epam.esm.model.Tag;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Clase TagDao para interactuar con la base de datos y realizar operaciones CRUD en la tabla 'tag'.
 */

@Component
public class TagDao {
    private JdbcTemplate jdbcTemplate;
    /**
     * Constructor de TagDao.
     *
     * @param jdbcTemplate El objeto JdbcTemplate para interactuar con la base de datos.
     */
    public TagDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Busca un tag por su nombre.
     *
     * @param name El nombre del tag a buscar.
     * @return El tag si se encuentra, null en caso contrario.
     */
    public Tag findByName(String name) {
        String sql = "SELECT * FROM tags WHERE name = ?";
        List<Tag> tags = jdbcTemplate.query(sql,
                ps -> ps.setString(1, name),
                (rs, rowNum) -> Tag.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .build());

        return tags.isEmpty() ? null : tags.get(0);

    }

    /**
     * Busca tags cuyo nombre contenga la cadena de texto proporcionada.
     *
     * @param namePart La cadena de texto a buscar en los nombres de los tags.
     * @return Una lista de tags cuyos nombres contienen la cadena de texto proporcionada.
     */

    public List<Tag> findByNamePart(String namePart) {
        String sql = "SELECT * FROM tag WHERE name LIKE ?";
        return jdbcTemplate.query(sql,
                ps -> ps.setString(1, "%" + namePart + "%"),
                (rs, rowNum) -> Tag.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .build());
    }
    /**
     * Obtiene todos los tags ordenados por nombre.
     *
     * @param order El orden de la clasificación ("ASC" para ascendente, "DESC" para descendente).
     * @return Una lista de todos los tags ordenados por nombre.
     */

    public List<Tag> findAllOrderedByName(String order) {
        String sql = "SELECT * FROM tag ORDER BY name " + order;
        return jdbcTemplate.query(sql, (rs, rowNum) -> Tag.builder()
                .id(rs.getInt("id"))
                .name(rs.getString("name"))
                .build());
    }


}