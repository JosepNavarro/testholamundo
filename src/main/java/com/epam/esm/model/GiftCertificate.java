package com.epam.esm.model;

import lombok.Builder;
import lombok.Value;

import java.util.Date;

@Value
@Builder

public class GiftCertificate {
    private int id;
    @Builder.Default
    private String name = "";
    @Builder.Default
    private String description= "";
    private int duration;
    @Builder.Default
    private Date createDate = new Date();
    @Builder.Default
    private Date lastUpdateDate = new Date();
}
