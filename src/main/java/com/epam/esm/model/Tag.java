package com.epam.esm.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Tag {
    private int id;
    @Builder.Default
    private String name = "";
}
