package com.epam.esm.config;

import com.epam.esm.dao.GiftCertificateDao;
import com.epam.esm.model.GiftCertificate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Date;

public class Main {
    public static void main(String[] args) {

        ApplicationContext context = new FileSystemXmlApplicationContext(
                "src/main/java/com/epam/esm/config/springConfig.xml");
        GiftCertificateDao giftCertificateDao = context.getBean(GiftCertificateDao.class);

        giftCertificateDao.deleteById(6);

        /*GiftCertificate giftCertificate = GiftCertificate.builder()
                .id(1)
                .name("Certificado 1 Perico modificado")
                .description("Perico era un buen programador")
                .duration(30)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();

        // Intenta actualizar el GiftCertificate en la base de datos
        try {
            giftCertificateDao.update(giftCertificate);
            System.out.println("GiftCertificate actualizado exitosamente");
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println(giftCertificateDao.findAllGiftCerticates());


    }
        GiftCertificate giftCertificate = GiftCertificate.builder()
                .name("Certificado 6 Juan Ramón")
                .description("JuanRa era comunicador")
                .duration(25)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();

        try {
            giftCertificateDao.insert(giftCertificate);
            System.out.println("GiftCertificate actualizado exitosamente");
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }

        System.out.println(giftCertificateDao.findAllGiftCerticates());

    }*/
    }
}