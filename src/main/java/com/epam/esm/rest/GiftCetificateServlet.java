package com.epam.esm.rest;

import com.epam.esm.dao.GiftCertificateDao;
import com.epam.esm.model.GiftCertificate;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet(name = "GiftCertificateServlet", value = {"/GiftCertificate", "/GiftCertificate/*"})
public class GiftCetificateServlet extends HttpServlet {

    @Autowired
    private GiftCertificateDao giftCertificateDao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Gson gson = new Gson();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            List<GiftCertificate> giftCertificates = giftCertificateDao.findAllGiftCerticates();
            String json = gson.toJson(giftCertificates);
            response.getWriter().write(json);
        } else {
            try {
                String idStr = pathInfo.substring(1);
                int id = Integer.parseInt(idStr);
                Optional<GiftCertificate> giftCertificate = giftCertificateDao.findById(id);
                if (giftCertificate.isPresent()) {
                    String json = gson.toJson(giftCertificate.get());
                    response.getWriter().write(json);
                } else {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND, "GiftCertificate no encontrado");
                }
            } catch (NumberFormatException e) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "ID no válido");
            }
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Gson gson = new Gson();
        GiftCertificate giftCertificate = gson.fromJson(request.getReader(), GiftCertificate.class);

        try {
            giftCertificateDao.insert(giftCertificate);
            response.getWriter().write("GiftCerticate agregado exitosamente");
        } catch (IllegalArgumentException e) {
            response.getWriter().write("Error" + e.getMessage());
        }
    }

    public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Gson gson = new Gson();
        GiftCertificate giftCertificate = gson.fromJson(request.getReader(), GiftCertificate.class);

        try {
            giftCertificateDao.update(giftCertificate);
            response.getWriter().write("GiftCerticate actualizado exitosamente");
        } catch (IllegalArgumentException e) {
            response.getWriter().write("Error" + e.getMessage());
        }
    }

    public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String pathInfo = request.getPathInfo();

        if(pathInfo == null || pathInfo.equals("/")) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "ID no proporcionado");
        } else {
            try {
                String idStr = pathInfo.substring(1);
                int id = Integer.parseInt(idStr);
                if(giftCertificateDao.deleteById(id) > 0) {
                    response.getWriter().write("GiftCertificate eliminado exitosamente");
                } else {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND, "GiftCertificate no encontrado");
                }
            } catch (NumberFormatException e) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "ID no válido");
            }
        }
    }


}
