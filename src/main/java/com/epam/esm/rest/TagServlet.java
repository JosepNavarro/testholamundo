package com.epam.esm.rest;

import com.epam.esm.dao.TagDao;
import com.epam.esm.model.Tag;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "TagServlet", value = "/Tags")
public class TagServlet extends HttpServlet {
    @Autowired
    private TagDao tagDao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String action = request.getParameter("action");
        Gson gson = new Gson();

        if ("findByName".equals(action)) {
            String name = request.getParameter("name");
            Tag tag = tagDao.findByName(name);
            if (tag != null) {
                response.getWriter().write(gson.toJson(tag));
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Tag no encontrado");
            }
        } else if ("findByNamePart".equals(action)) {
            String namePart = request.getParameter("namePart");
            List<Tag> tags = tagDao.findByNamePart(namePart);
            if (!tags.isEmpty()) {
                response.getWriter().write(gson.toJson(tags));
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Tags no encontrados");
            }
        } else if ("findAllOrderedByName".equals(action)) {
            String order = request.getParameter("order");
            List<Tag> tags = tagDao.findAllOrderedByName(order);
            if (!tags.isEmpty()) {
                response.getWriter().write(gson.toJson(tags));
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Tags no encontrados");
            }
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Acción no válida");
        }
    }
}

