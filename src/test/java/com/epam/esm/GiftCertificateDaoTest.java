package com.epam.esm;

import com.epam.esm.dao.GiftCertificateDao;
import com.epam.esm.model.GiftCertificate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.testng.Assert;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class GiftCertificateDaoTest {

    @Mock
    private JdbcTemplate jdbcTemplate;


    private GiftCertificateDao giftCertificateDao;

    @BeforeEach
    public void setUp() {
        giftCertificateDao = new GiftCertificateDao(jdbcTemplate);
    }

    @Test
    public void testFindById() {
        int id = 1;
        GiftCertificate expectedCertificate = GiftCertificate.builder()
                .id(id)
                .name("Certificado Test")
                .description("Descripcion del certificado test")
                .duration(30)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();
        List<GiftCertificate> expectedList = Collections.singletonList(expectedCertificate);


        when(jdbcTemplate.query(
                eq("SELECT * FROM gift_certificate WHERE id = ?"),
                any(PreparedStatementSetter.class),
                any(RowMapper.class)))
                .thenReturn(expectedList);

        Optional<GiftCertificate> actualCertificate = giftCertificateDao.findById(id);

        Assert.assertNotNull(actualCertificate);
        Assert.assertTrue(actualCertificate.isPresent());
        Assert.assertEquals(expectedCertificate, actualCertificate.get());

    }
    @Test
    public void testInsert() {
        GiftCertificate giftCertificate = GiftCertificate.builder()
                .name("Certificado Test")
                .description("Descripcion del certificado test")
                .duration(30)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();
        String sql = "INSERT INTO gift_certificate (name, description, duration, create_date, last_update_date) VALUES (?, ?, ?, ?, ?)";
        when(jdbcTemplate.update(
                sql,
                giftCertificate.getName(),
                giftCertificate.getDescription(),
                giftCertificate.getDuration(),
                giftCertificate.getCreateDate(),
                giftCertificate.getLastUpdateDate()))
                .thenReturn(1);

        int rowsAffected = giftCertificateDao.insert(giftCertificate);
        verify(jdbcTemplate).update(sql,giftCertificate.getName(),
                giftCertificate.getDescription(),
                giftCertificate.getDuration(),
                giftCertificate.getCreateDate(),
                giftCertificate.getLastUpdateDate());
        assertEquals(1, rowsAffected);
    }
    @Test
    public void testUpdate() {
        GiftCertificate giftCertificate = GiftCertificate.builder()
                .id(1)
                .name("Certificado Test Actualizado")
                .description("Descripcion del certificado test actualizado")
                .duration(30)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();

        String sql = "UPDATE gift_certificate SET name = ?, description = ?, duration = ?, create_date = ?, last_update_date = ? WHERE id = ?";

        when(jdbcTemplate.update(sql, giftCertificate.getName(), giftCertificate.getDescription(), giftCertificate.getDuration(), giftCertificate.getCreateDate(), giftCertificate.getLastUpdateDate(), giftCertificate.getId()))
                .thenReturn(1);

        int rowsAffected = giftCertificateDao.update(giftCertificate);
        assertEquals(1, rowsAffected);
    }
    @Test
    public void testDeleteById() {
        int id = 1;

        String sql = "DELETE FROM gift_certificate WHERE id = ?";

        when(jdbcTemplate.update(sql, id))
                .thenReturn(1);

        int rowsAffected = giftCertificateDao.deleteById(id);

        assertEquals(1, rowsAffected);
    }
    @Test
    public void testInsertWithLongName() {
        String name = String.join("", Collections.nCopies(256, "a"));
        GiftCertificate giftCertificate = GiftCertificate.builder()
                .name(name)
                .description("Descripcion del certificado test")
                .duration(30)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();

        assertThrows(IllegalArgumentException.class, () -> giftCertificateDao.insert(giftCertificate));
    }
    @Test
    public void testUpdateWithLongName() {
        String name = String.join("", Collections.nCopies(256, "a"));
        GiftCertificate giftCertificate = GiftCertificate.builder()
                .id(1)
                .name(name)
                .description("Descripcion del certificado test")
                .duration(30)
                .createDate(new Date())
                .lastUpdateDate(new Date())
                .build();

        assertThrows(IllegalArgumentException.class, () -> giftCertificateDao.update(giftCertificate));
    }

}

