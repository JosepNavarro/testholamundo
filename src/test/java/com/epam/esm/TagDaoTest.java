package com.epam.esm;


import com.epam.esm.dao.TagDao;
import com.epam.esm.model.Tag;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TagDaoTest {

    @Mock
    private JdbcTemplate jdbcTemplate;


    private TagDao tagDao;

    @BeforeEach
    public void setUp() {
        tagDao = new TagDao(jdbcTemplate);
    }

    @Test
    public void testFindByName() {
        String name = "Tag Test";
        Tag expectedTag = Tag.builder()
                .id(1)
                .name(name)
                .build();

        when(jdbcTemplate.query(
                eq("SELECT * FROM tags WHERE name = ?"),
                any(PreparedStatementSetter.class),
                any(RowMapper.class)))
                .thenReturn(Collections.singletonList(expectedTag));

        Tag actualTag = tagDao.findByName(name);

        assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testFindByNamePart() {
        String namePart = "Tag";
        List<Tag> expectedTags = Arrays.asList(
                Tag.builder().id(1).name("Tag Test 1").build(),
                Tag.builder().id(2).name("Tag Test 2").build());

        when(jdbcTemplate.query(
                eq("SELECT * FROM tag WHERE name LIKE ?"),
                any(PreparedStatementSetter.class),
                any(RowMapper.class)))
                .thenReturn(expectedTags);

        List<Tag> actualTags = tagDao.findByNamePart(namePart);

        assertEquals(expectedTags, actualTags);
    }

    @Test
    public void testFindAllOrderedByName() {
        String order = "ASC";
        List<Tag> expectedTags = Arrays.asList(
                Tag.builder().id(1).name("Tag Test 1").build(),
                Tag.builder().id(2).name("Tag Test 2").build());

        when(jdbcTemplate.query(
                eq("SELECT * FROM tag ORDER BY name " + order),
                any(RowMapper.class)))
                .thenReturn(expectedTags);

        List<Tag> actualTags = tagDao.findAllOrderedByName(order);

        assertEquals(expectedTags, actualTags);
    }
}

